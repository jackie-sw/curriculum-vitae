function random(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
//随机生成颜色
function randomColor() {
  return (
    "rgba(" +
    random(0, 255) +
    "," +
    random(0, 255) +
    "," +
    random(0, 255) +
    ")"
  );
}
/** @type {HTMLCanvasElement} */
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const width = canvas.width = 800;
const height = canvas.height = 500;
const content = document.getElementById('ball-num');
let balls = [];
let ballNum = 0;
//形状类
function Shape(x, y, speedX, speedY, exist) {
  this.x = x;
  this.y = y;
  this.speedX = speedX;
  this.speedY = speedY;
  this.exist = exist;
}
//小球对象
function Ball(x, y, speedX, speedY, exist, color, size) {
  Shape.call(this, x, y, speedX, speedY, exist);
  this.color = color;
  this.size = size;
}
//绘制小球
Ball.prototype.draw = function () {
  ctx.save();
  ctx.beginPath();
  ctx.fillStyle = this.color;
  ctx.arc(this.x, this.y, this.size, 0, (360 * Math.PI) / 180);
  ctx.fill();
  ctx.restore();
};
//更新小球的位置
Ball.prototype.update = function () {
  if (this.x + this.size >= width) {
    this.speedX = -this.speedX;
  }
  if (this.x - this.size <= 0) {
    this.speedX = -this.speedX;
  }
  if (this.y + this.size >= height) {
    this.speedY = -this.speedY;
  }
  if (this.y - this.size <= 0) {
    this.speedY = -this.speedY;
  }
  this.x += this.speedX;
  this.y += this.speedY;
};
//碰撞检测
Ball.prototype.collisionDetect = function () {
  for (let i = 0; i < balls.length; i++) {
    if (this !== balls[i]) {
      const dx = this.x - balls[i].x;
      const dy = this.y - balls[i].y;
      const distance = Math.sqrt(dx * dx + dy * dy);
      if (distance < this.size + balls[i].size) {
        balls[i].color = this.color = randomColor();
      }
    }
  }
}
//消除圈对象
function EvilCircle(x, y, speedX, speedY, exist) {
  //继承
  Shape.call(this, x, y, exist);
  this.speedX = speedX;
  this.speedY = speedY;
  this.color = 'white';
  this.size = 20;
  this.lineWidth = 10;
}
let evil = new EvilCircle(10, 10, 10, 10, true);
//绘制消球圈
EvilCircle.prototype.draw = function () {
  ctx.save();
  ctx.beginPath();
  ctx.lineWidth = this.lineWidth;
  ctx.strokeStyle = this.color;
  ctx.arc(this.x, this.y, this.size, 0, 360 * Math.PI / 180);
  ctx.stroke();
  ctx.restore();
}
//判断是否触边
EvilCircle.prototype.checkBounds = function () {
  if (this.x + this.size >= width) {
    this.x -= this.size - this.lineWidth;
  } else if (this.x - this.size <= 0) {
    this.x += this.size - this.lineWidth;
  } else if (this.y + this.size >= height) {
    this.y -= this.size - this.lineWidth;
  } else if (this.y - this.size <= 0) {
    this.y += this.size - this.lineWidth;
  }
}
//键盘控制小球
EvilCircle.prototype.setControls = function () {
  let _this = this;
  let left = false;
  let right = false;
  let top = false;
  let bottom = false;
  setInterval(function () {
    if (left) {
      _this.x -= _this.speedX;
    } else if (right) {
      _this.x += _this.speedX;
    } else if (top) {
      _this.y -= _this.speedY;
    } else if (bottom) {
      _this.y += _this.speedY;
    }
  }, 10)
  window.onkeydown = function (e) {
    let ev = e || event;
    switch (ev.key) {
      case 'a':
        left = true;
        break;
      case 'd':
        right = true;
        break;
      case 's':
        bottom = true;
        break;
      case 'w':
        top = true;
        break;
      default:
        break;
    }
  }
  window.onkeyup = function (e) {
    let ev = e || event;
    switch (ev.key) {
      case 'a':
        left = false;
        break;
      case 'd':
        right = false;
        break;
      case 's':
        bottom = false;
        break;
      case 'w':
        top = false;
        break;
      default:
        break;
    }
  }
}
//碰撞检测
EvilCircle.prototype.collisionDetect = function () {
  for (let i = 0; i < balls.length; i++) {
    if (balls[i].exist) {
      const dx = this.x - balls[i].x;
      const dy = this.y - balls[i].y;
      const distance = Math.sqrt(dx * dx + dy * dy);
      if (distance < this.size + balls[i].size) {
        balls[i].exist = false;
        ballNum--;
      }
    }
  }
}

while (balls.length < 20) {
  let size = random(10, 20);
  let ball = new Ball(
    // 为避免绘制错误，球至少离画布边缘球本身一倍宽度的距离
    random(0 + size, width - size),
    random(0 + size, height - size),
    random(-7, 7),
    random(-7, 7),
    true,
    randomColor(),
    size
  );
  ballNum++;
  content.innerHTML = ballNum;
  balls.push(ball);
}

evil.setControls();
let intervalGO = 0;
function gameOver() {
  cancelAnimationFrame(loop)
  ctx.clearRect(0, 0, width, height);
  ctx.fillRect(0, 0, width, height);
  ctx.save();
  intervalGO = setInterval(function () {
    ctx.globalAlpha += 0.1;
    if (ctx.globalAlpha >= 1) {
      ctx.globalAlpha = 1;
      clearInterval(intervalGO);
    }
    ctx.beginPath();
    ctx.fillStyle = '#b0b0b0';
    ctx.fillRect(0, 0, width, height);
    ctx.beginPath();
    ctx.fillStyle = 'red';
    ctx.font = "bold 40px ''";
    ctx.fillText('Game Over', width / 2 - 100, height / 2 - 40);
    ctx.fill();
  }, 1000)
  ctx.restore();
  clearInterval(intervalId);
}
//循环绘制
function loop() {
  ctx.fillStyle = "rgba(0, 0, 0, 0.25)";
  ctx.fillRect(0, 0, width, height);
  for (let i = 0; i < balls.length; i++) {
    if (balls[i].exist) {
      balls[i].draw();
      balls[i].update();
      balls[i].collisionDetect();
    }
  }
  evil.draw();
  evil.checkBounds();
  evil.collisionDetect();
  content.innerHTML = ballNum;
  if (ballNum === 0) {
    gameOver();
  }
  requestAnimationFrame(loop);
}
loop();