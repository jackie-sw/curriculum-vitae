// 图片全部加载完成后才可以打印
function getLoadPromise(dom) {
    let imgs = dom.querySelectorAll("img");
    imgs = [].slice.call(imgs);
    if (imgs.length === 0) {
        return Promise.resolve();
    }
    let finishedCount = 0;
    return new Promise((resolve) => {
        function check() {
            finishedCount++;
            if (finishedCount === imgs.length) {
                resolve();
            }
        }
        imgs.forEach((img) => {
            img.addEventListener("load", check);
            img.addEventListener("error", check);
        });
    });
}
//打印
function printResume() {
    const bdHtml = window.document.body.innerHTML;
    const start = "<!-- startprint -->";
    const end = "<!-- endprint -->";
    const printHtml = bdHtml.substring(
        bdHtml.indexOf(start) + 19,
        bdHtml.indexOf(end)
    );
    const container = document.createElement("div")
    container.innerHTML = printHtml
    getLoadPromise(container).then(() => {
        window.print();
        window.onbeforeprint = function () {
            window.document.body.innerHTML = printHtml;
        };
        window.onafterprint = function () {
            window.document.body.innerHTML = bdHtml;
        };
    })

    return false;
}