(function () {
  window.drag = function (obj, callback) {
    //元素的开始位置
    var elePoint = { x: 0, y: 0 };
    //鼠标一开始的位置
    var mousPoint = { x: 0, y: 0 };

    obj.onmousedown = function (e) {
      e = e || window.event;
      //获取元素的开始位置
      elePoint.x = this.offsetLeft;
      elePoint.y = this.offsetTop;
      //获取鼠标的开始位置
      mousPoint.x = e.clientX;
      mousPoint.y = e.clientY;
      //捕获事件
      if (obj.setCapture) {
        obj.setCapture();
      }
      document.onmousemove = function (e) {
        e = e || window.event;
        //获取移动时的鼠标的位置
        var nowPoint = { x: 0, y: 0 };
        nowPoint.x = e.clientX;
        nowPoint.y = e.clientY;
        //obj移动后的位置
        var L = elePoint.x + nowPoint.x - mousPoint.x;
        var T = elePoint.y + nowPoint.y - mousPoint.y;

        //接口

        if (
          callback &&
          callback["moveL"] &&
          typeof callback["moveL"] === "function"
        ) {
          //超出事件原来的位置就置为0
          if (L < 0) {
            L = 0;
            //offsetParent为最近参考的定位元素，也就是relative
            //超出对应范围就置为最大值
          } else if (L > obj.offsetParent.offsetWidth - obj.clientWidth) {
            L = obj.offsetParent.offsetWidth - obj.clientWidth;
          }
          //设置滑动块的
          obj.style.left = L + "px";
          callback["moveL"].call(obj);
        }
        if (
          callback &&
          callback["moveT"] &&
          typeof callback["moveT"] === "function"
        ) {
          //超出事件原来的位置就置为0
          if (T < 0) {
            T = 0;
            //offsetParent为最近参考的定位元素，也就是relative
            //超出对应范围就置为最大值
          } else if (T > obj.parentNode.offsetHeight - obj.clientHeight) {
            T = obj.parentNode.offsetHeight - obj.clientHeight;
          }
          //设置滑动块的
          obj.style.top = T + "px";
          callback["moveT"].call(obj);
        }
      };
      //不按了以后释放捕获
      document.onmouseup = function () {
        document.onmousemove = document.onmouseup = null;
        if (document.releaseCaputre) {
          document.releaseCaputre();
        }
      };
      return false;
    };
  };
})(window);