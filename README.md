# jackie-sw_个人简历

#### 介绍
​	个人简历网站，主要是进行自我介绍，放置一些自己的做过的项目等等。

​	你可以通过		 [jackie-sw个人简历](http://jackie-sw.gitee.io/curriculum-vitae/)		进行在线访问查看。

#### 软件架构
​	html、js、css、jQuery、Bootstrap

#### 使用说明

​	首先index.html是存放个人简历主页的，如果想要更换个人资料卡的内容可以在#main下找到.card底下的内容进行修改。

​	如果想要更改联系qq号（需要将href的uin改为自己的qq号）

```html
<a
   href="tencent://message/?uin=252209094"
   class="contact-items"
   data-toggle="tooltip"
   title="联系我：252209094"
   ><i class="fab fa-qq" aria-hidden="true"></i>
</a>
```

​	如果想要改变职业技能方面的信息。需要注意

​	row-title标签底下的为技能名，progress为进度条，

​	通过style对进度条的大小进行更改

​	通过title更换提示框的内容

```html
<div class="row-title">
    <strong>HTML</strong>
</div>
<div class="progress">
    <div
         class="progress-bar"
         role="progressbar"
         style="width: 95%"
         aria-valuenow="25"
         aria-valuemin="0"
         aria-valuemax="100"
         >
        <div
             class="btn btn-danger"
             data-toggle="tooltip"
             title="精通"
             >
            95%
        </div>
    </div>
</div>
```

​	如果想要改变项目中的内容，可以直接将#main中的内容进行替换，换上自己的。